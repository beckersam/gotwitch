/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package gotwitch

import (
	"gitlab.com/beckersam/gotwitch/bot"
	"gitlab.com/beckersam/gotwitch/events"
	"gitlab.com/beckersam/gotwitch/webserver"
	"log"
	"sync"
)

// BotWrapper is a container for the Twitch client and the webserver, serving as a transport layer.
type BotWrapper struct {
	bot       *bot.CommandHandler
	ws        *webserver.Server
	wg        *sync.WaitGroup
	eventChan chan events.Event
}

// Start starts both the Twitch client and the webserver
func (bw *BotWrapper) Start() error {
	errChan := make(chan error)
	bw.wg.Add(2)
	go bw.bot.Start(errChan)
	go bw.ws.Start(errChan)

	defer func(b *bot.CommandHandler) {
		err := b.Stop()
		if err != nil && err != bot.ErrAlreadyStopped {
			log.Fatalf("Failed to shut down Twitch bot properly. Err: %v", err)
		}
	}(bw.bot)
	defer func(ws *webserver.Server) {
		err := ws.Stop()
		if err != nil && err != webserver.ErrAlreadyClosed {
			log.Fatalf("Failed to shut down the webserver properly. Err: %v", err)
		}
	}(bw.ws)

	bw.wg.Wait()

	return <-errChan
}

// NewBot creates a new wrapper using the given authentication data.
// This also initialises both the Twitch client and the webserver.
func NewBot(auth *bot.AuthData) *BotWrapper {
	eventChan := make(events.EventChan)
	wg := &sync.WaitGroup{}
	return &BotWrapper{
		wg:        wg,
		bot:       bot.NewHandler(auth, eventChan),
		ws:        webserver.NewServer(wg, eventChan),
		eventChan: eventChan,
	}
}
