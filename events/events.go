/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package events

import "time"

// Event defines a generic interface to pass events from the Twitch client to the webserver.
type Event interface {
	GetUser() string
	GetText() string
	GetType() EventType
	GetTimestamp() time.Time
}

// SubEvent describes events triggered by a subscription
type SubEvent interface {
	Event
	GetTier() SubTier
}

// BitEvent describes events triggered by a bit donation.
type BitEvent interface {
	Event
	GetBitCount() uint
}
