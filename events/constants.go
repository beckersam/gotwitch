/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package events

type EventChan chan Event
type EventType int
type SubTier int

const (
	EventSub = EventType(iota)
	EventFollow
	EventBits
)
const (
	SubTier1 = SubTier(iota)
	SubTier2
	SubTier3
	SubTierPrime
)
