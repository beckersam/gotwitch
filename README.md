# IMPORTANT
Stopping development of this project for now. 
Reason is that I want to make a more complete and thought out version
accompassing not just Twitch, but also YouTube Live, OwnCast and other streaming services. 

# gotwitch
A framework designed to help with building bots for Twitch.
Even includes a webserver for notifactions on stream

There are 3 types of commands:
- Locked: Commands that are implemented into the core of the bot and can't be disabled/overwritten/etc
- Custom: Commands that execute some function that may or may not return anything. Implemented by the user
- Info: Commands that return a static text. Those can be set by the user
