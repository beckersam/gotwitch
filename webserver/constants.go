package webserver

import "errors"

var ErrAlreadyClosed = errors.New("server already closed")
