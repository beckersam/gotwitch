/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package webserver

import (
	"context"
	"gitlab.com/beckersam/gotwitch/events"
	"gitlab.com/beckersam/goutils/pkg/containers"
	"net/http"
	"sync"
	"time"
)

// Server contains all information the webserver needs to operate
type Server struct {
	srv        *http.Server
	wg         *sync.WaitGroup
	closed     bool
	eventChan  events.EventChan
	eventQueue *containers.Queue[events.Event]
}

// NewServer generates and initialises a new Server struct.
// wg is used for managing start and stop.
// eventChan is used for reading incoming events to display
func NewServer(wg *sync.WaitGroup, eventChan events.EventChan) *Server {
	srv := &http.Server{Addr: ":8000"}
	fs := http.FileServer(http.Dir("./webserver/static/"))
	http.Handle("/", fs)
	return &Server{
		srv:        srv,
		wg:         wg,
		closed:     false,
		eventChan:  eventChan,
		eventQueue: containers.BuildQueue[events.Event](),
	}
}

// Start starts the server.
// Will fail if the server has been stopped before.
// As this is expected to be run as a goroutine, errChan is used to return any errors.
func (s *Server) Start(errChan chan error) {
	if err := s.srv.ListenAndServe(); err != http.ErrServerClosed {
		// Throw any non-expected errors back to caller
		errChan <- err
	}
	defer s.wg.Done()
}

// Stop stops the server and returns possible errors.
// If the server has been stopped before it'll return ErrAlreadyClosed
func (s *Server) Stop() error {
	if s.closed {
		return ErrAlreadyClosed
	}
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*10)
	defer cancel()
	return s.srv.Shutdown(ctx)
}

//TODO: Add shutdown handling a la https://stackoverflow.com/a/42533360
