package webserver

import (
	"fmt"
	"net/http"
)

func routeRoot(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprint(w, "Root web page")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
