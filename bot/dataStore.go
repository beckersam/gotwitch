/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import "strings"

type dataStore struct {
	ints        map[string]int
	strings     map[string]string
	floats      map[string]float32
	bannedUsers map[string]bool
}

func newDataStore() *dataStore {
	return &dataStore{
		ints:        map[string]int{},
		strings:     map[string]string{},
		floats:      map[string]float32{},
		bannedUsers: map[string]bool{},
	}
}

func (ds *dataStore) StoreInt(key string, value int) {
	ds.ints[key] = value
}

func (ds *dataStore) StoreString(key, value string) {
	ds.strings[key] = value
}

func (ds *dataStore) StoreFloat32(key string, value float32) {
	ds.floats[key] = value
}

func (ds *dataStore) LoadInt(key string) int {
	return ds.ints[key]
}

func (ds *dataStore) LoadString(key string) string {
	return ds.strings[key]
}

func (ds *dataStore) LoadFloat32(key string) float32 {
	return ds.floats[key]
}

func (ds *dataStore) IsBanned(name string) bool {
	banned := ds.bannedUsers[strings.ToLower(name)]
	return banned
}

func (ds *dataStore) AddBanned(name string) {
	ds.bannedUsers[strings.ToLower(name)] = true
}

func (ds *dataStore) RemoveBanned(name string) {
	ds.bannedUsers[strings.ToLower(name)] = false
}
