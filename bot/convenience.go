/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import "github.com/gempir/go-twitch-irc/v3"

// Reply replies to a given message using a given text.
func (ch *CommandHandler) Reply(msg twitch.PrivateMessage, text string) {
	ch.Client.Reply(msg.Channel, msg.ID, text)
}
