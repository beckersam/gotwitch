/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import "github.com/gempir/go-twitch-irc/v3"

// GetLockedCommands returns a map containing all predefined commands.
// Modifications to the result *should* not matter as the bot includes them on initialisation.
func GetLockedCommands() map[string]*Command {
	return map[string]*Command{
		"ping": {
			key:         "ping",
			minArgs:     0,
			maxArgs:     0,
			perms:       PermAll,
			Description: "Ping the living shit out of the bot to see it it's still alive",
			runner: func(pm twitch.PrivateMessage, s []string, ch *CommandHandler) {
				ch.Reply(pm, "pong")
			},
		},
		"source": {
			key:         "source",
			minArgs:     0,
			maxArgs:     0,
			perms:       PermAll,
			Description: "Link to the source of the bot's librarby",
			runner: func(pm twitch.PrivateMessage, s []string, ch *CommandHandler) {
				ch.Reply(pm, "https://gitlab.com/beckersam/gotwitch")
			},
		},
		"devsay": {
			key:         "devsay",
			minArgs:     1,
			maxArgs:     1,
			perms:       PermFrameworkDev,
			Description: "Dev can say something with the highest priority",
			runner: func(pm twitch.PrivateMessage, s []string, ch *CommandHandler) {
				// TODO: Once webserver with notifications is ready, link this to a priority channel
				ch.Reply(pm, s[0])
			},
		},
	}
}
