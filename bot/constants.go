/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import "errors"

// Permission flags for commands
const (
	PermFrameworkDev = CommandPermission(iota)
	PermBotOwner
	PermStreamer
	PermMod
	PermVip
	PermSub
	// PERM_FOLLOWER // Disabled b/c not capable of checking this yet
	PermAll
)

var ErrAlreadyStopped = errors.New("bot not running")
