/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import (
	"gitlab.com/beckersam/gotwitch/events"
	"log"
	"strings"

	"github.com/gempir/go-twitch-irc/v3"
)

// CommandHandler contains all data that is required for the Twitch bot part of the system.
// This includes the actual client, authentication data and the various registered commands.
type CommandHandler struct {
	Client               *twitch.Client
	auth                 *AuthData
	data                 *dataStore
	Commands             map[string]*Command
	locked               map[string]*Command
	Infos                map[string]string
	eventChan            events.EventChan
	customMessageHandler func(*CommandHandler, twitch.PrivateMessage)
	stopped              bool
}

// NewHandler generates a new Twitch bot handler.
// auth is the login data used to connect to Twitch.
// eventChan is the channel where events such as subscriptions are sent to.
func NewHandler(auth *AuthData, eventChan events.EventChan) *CommandHandler {
	client := twitch.NewClient(auth.Name, auth.Token)

	handler := &CommandHandler{
		Client:               client,
		auth:                 auth,
		data:                 newDataStore(),
		Commands:             make(map[string]*Command),
		locked:               make(map[string]*Command),
		Infos:                make(map[string]string),
		eventChan:            eventChan,
		customMessageHandler: func(_ *CommandHandler, _ twitch.PrivateMessage) {},
		stopped:              false,
	}

	client.OnConnect(func() { log.Println("Bot connected") })
	client.OnPrivateMessage(handler.handleChatMessage)
	return handler
}

// NewHandlerWithCustomChatRunner generates a new Twitch bot handler.
// auth is the login data used to connect to Twitch.
// eventChan is the channel where events such as subscriptions are sent to.
// cmh is a function that will be executed every time a message is received and not decoded into a command.
func NewHandlerWithCustomChatRunner(auth *AuthData, eventChan events.EventChan, cmh func(*CommandHandler, twitch.PrivateMessage)) *CommandHandler {
	client := twitch.NewClient(auth.Name, auth.Token)

	handler := &CommandHandler{
		Client:               client,
		auth:                 auth,
		Commands:             make(map[string]*Command),
		locked:               make(map[string]*Command),
		Infos:                make(map[string]string),
		eventChan:            eventChan,
		customMessageHandler: cmh,
	}

	client.OnConnect(func() { log.Println("Bot connected") })
	client.OnPrivateMessage(handler.handleChatMessage)
	return handler
}

// Start starts up the bot.
// Since this function is expected to be run as a goroutine, errChan will be used to return possible errors.
func (ch *CommandHandler) Start(errChan chan error) {
	err := ch.Client.Connect()
	// Connect is blocking. Once it exits we can set the client to stopped
	ch.stopped = true
	errChan <- err
}

// Stop stops the bot and prevents another startup.
// Returns ErrAlreadyStopped if called after the bot has been stopped.
func (ch *CommandHandler) Stop() error {
	if ch.stopped {
		return ErrAlreadyStopped
	}
	ch.stopped = true
	return nil
}

// WasStopped returns true if the bot has been stopped beforehand.
func (ch *CommandHandler) WasStopped() bool {
	return ch.stopped
}

// handleChatMessage is the handler that interprets any normal chat messages received from Twitch.
// It is not intended for use outside of that purpose
func (ch *CommandHandler) handleChatMessage(message twitch.PrivateMessage) {
	// Is the bot the sender?
	if strings.ToLower(message.User.Name) == ch.auth.Name {
		return
	}
	// Is author on internal ban list?
	if ch.data.IsBanned(message.User.Name) {
		return
	}

	// If message is a command, try to run it
	if len(message.Message) >= len(ch.auth.Prefix) &&
		strings.HasPrefix(message.Message, ch.auth.Prefix) {
		err := ch.startCommandChain(message)
		if err != nil {
			log.Println(err)
			ch.Client.Reply(message.Channel, message.ID, "Error processing your request")
		}
	} else {
		ch.customMessageHandler(ch, message)
	}
}

func (ch *CommandHandler) StoreInt(key string, value int) {
	ch.data.StoreInt(key, value)
}

func (ch *CommandHandler) StoreString(key, value string) {
	ch.data.StoreString(key, value)
}

func (ch *CommandHandler) StoreFloat32(key string, value float32) {
	ch.data.StoreFloat32(key, value)
}

func (ch *CommandHandler) LoadInt(key string) int {
	return ch.data.LoadInt(key)
}

func (ch *CommandHandler) LoadString(key string) string {
	return ch.data.LoadString(key)
}

func (ch *CommandHandler) LoadFloat32(key string) float32 {
	return ch.data.LoadFloat32(key)
}
