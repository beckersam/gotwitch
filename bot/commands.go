/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import "github.com/gempir/go-twitch-irc/v3"

type CommandPermission int

// Command contains all information necessary to run the contained command
type Command struct {
	key         string
	Description string
	minArgs     int
	maxArgs     int
	perms       CommandPermission
	runner      func(twitch.PrivateMessage, []string, *CommandHandler)
}

// GetKey is a getter for the name of the command
func (c *Command) GetKey() string {
	return c.key
}

// GetMinArgs is a getter for the minimum number of arguments required for the that command.
func (c *Command) GetMinArgs() int {
	return c.minArgs
}

// GetMaxArgs is a getter for the maximum number of arguments a command can handle.
func (c *Command) GetMaxArgs() int {
	return c.maxArgs
}

// GetPerms is a getter for the permissions that are required to execute that command.
func (c *Command) GetPerms() CommandPermission {
	return c.perms
}

// Run executes the command, passing the raw message, arguments and a reference to the main bot on.
func (c *Command) Run(msg twitch.PrivateMessage, args []string, ch *CommandHandler) {
	c.runner(msg, args, ch)
}
