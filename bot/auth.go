/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import (
	"encoding/json"
	"io"
	"os"
	"strings"
)

// AuthData contains all information necessary to connect to Twitch.
type AuthData struct {
	Token    string `json:"token"`
	Refresh  string `json:"-"`
	ClientID string `json:"client"`
	Channel  string `json:"channel"`
	Prefix   string `json:"prefix"`
	Name     string `json:"name"`
	Dev      string `json:"dev,omitempty"`
}

// Decode can decode a file's content into a valid AuthData struct.
// Passes on any errors encountered.
func Decode(filename string) (*AuthData, error) {
	handle, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer handle.Close()

	var rawContent []byte
	rawContent, err = io.ReadAll(handle)
	if err != nil {
		return nil, err
	}
	var auth AuthData

	err = json.Unmarshal(rawContent, &auth)
	if err != nil {
		return nil, err
	}

	// Twitch uses "oauth:..." as the token format. Store it as such
	auth.Token = "oauth:" + auth.Token
	auth.Name = strings.ToLower(auth.Name)

	return &auth, nil
}
