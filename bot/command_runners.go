/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package bot

import (
	"errors"
	"strings"

	"github.com/gempir/go-twitch-irc/v3"
)

// startCommandChain takes a raw message and tries to run it as a command
func (ch *CommandHandler) startCommandChain(msg twitch.PrivateMessage) error {
	// Extract useful info from message
	tmp := strings.SplitN(msg.Message[len(ch.auth.Prefix):], " ", 1)

	// Catch invalid events
	if len(tmp) < 1 || len(tmp) > 2 {
		return errors.New("invalid message content")
	}

	if len(tmp) == 1 {
		tmp = append(tmp, "")
	}

	// Parse command
	cmdName := strings.ToLower(tmp[0])

	// Check if command is an internal one. Run it if yes
	ran, err := ch.checkAndRunCodeCmd(cmdName, tmp[1], msg, ch.locked)
	if err != nil {
		return err
	}
	if ran {
		return nil
	}

	// Check if command is a user coded one. Run if yes
	ran, err = ch.checkAndRunCodeCmd(cmdName, tmp[1], msg, ch.Commands)
	if err != nil {
		return err
	}
	if ran {
		return nil
	}

	if response, ok := ch.Infos[cmdName]; ok {
		ch.Reply(msg, response)
	}
	return nil
}

// checkAndRunCodeCmd checks if a command with a given name exists and runs it.
// It also checks for a valid argument count and user permissions
func (ch *CommandHandler) checkAndRunCodeCmd(cmdName, argsRaw string, msg twitch.PrivateMessage, cmdMap map[string]*Command) (bool, error) {
	if cmd, ok := cmdMap[cmdName]; ok {
		// Check permissions
		if !ch.hasPerms(cmd, msg) {
			ch.Reply(msg, "You do not have permissions to run this command.")
			return true, nil
		}
		// Check argument count
		args := strings.SplitN(argsRaw, " ", cmd.GetMaxArgs())
		if len(args) < cmd.GetMinArgs() {
			ch.Reply(msg, "Not enough arguments.")
			return true, nil
		}
		if len(args) > cmd.GetMaxArgs() {
			ch.Reply(msg, "Too many arguments.")
			return true, nil
		}
		// Run command as goroutine.
		// Avoids blocking
		go cmd.Run(msg, args, ch)
		return true, nil
	}
	return false, nil
}

// hasPerms checks if the author of a given message has the permission to execute a certain command
func (ch *CommandHandler) hasPerms(cmd *Command, msg twitch.PrivateMessage) bool {
	reqPerms := cmd.GetPerms()

	// Case: Framework Owner (Myself) is executing the command
	if msg.User.Name == "MrEvilOnTwitch" {
		return reqPerms >= PermFrameworkDev
	}
	if msg.User.Name == ch.auth.Dev {
		return reqPerms >= PermBotOwner
	}
	// Case: Streamer / Twitch staff is executing the command
	if msg.User.Name == msg.Channel || msg.Tags["user-type"] != "" {
		return reqPerms >= PermStreamer
	}
	// Case: Chat Moderator is executing the command
	if msg.Tags["mod"] == "1" {
		return reqPerms >= PermMod
	}
	// Case: VIP is executing the command
	if msg.Tags["vip"] != "" {
		return reqPerms >= PermVip
	}
	// Case: Sub is executing the command
	if msg.Tags["subscriber"] == "1" {
		return reqPerms >= PermSub
	}
	return false
}
