module gitlab.com/beckersam/gotwitch

go 1.19

require github.com/gempir/go-twitch-irc/v3 v3.2.0

require gitlab.com/beckersam/goutils v0.0.0-20230119115146-c50cf88dd045 // indirect
