/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package main

import (
	"gitlab.com/beckersam/gotwitch/events"
	"gitlab.com/beckersam/gotwitch/webserver"
)

func main() {
	eventChan := make(events.EventChan)
	errChan := make(chan error)
	ws := webserver.NewServer(eventChan)
	go ws.Start(errChan)
	print(<-errChan)
}
